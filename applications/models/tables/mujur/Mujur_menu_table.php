<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Settings_table
 *
 * @author R700
 */
class Mujur_menu_table extends CI_Model {

    public $table;
    public $db_main;
    public $field_id;

    public function __CONSTRUCT() {
        parent::__construct();
        if (is_local()) {
            log_add('mujur_menu_table load ','table');
        }
        $this->table = 'menus';
        $this->field_id = 'menu_id';
        $this->db_main = $this->base_model->db_main;
        //$data = $this->created_table();
    }

    public function tablename() {
        return $this->db_main->dbprefix($this->table);
    }

    function builds() {
        if (ENVIRONMENT != 'development') {
            return TRUE;
        }
        $table = $this->table;
        $table_name = $this->db_main->dbprefix($table);
        if (!$this->db_main->table_exists($table)) {
            $forge = $this->load->dbforge($this->db_main, TRUE);
            //------------
            $fields = array(
                $this->field_id => array(
                    'type' => 'BIGINT',
                    'auto_increment' => TRUE
                ),
                'modified' => array(
                    'type' => 'timestamp',
                ),
                'created' => array(
                    'type' => 'datetime',
                    'default' => date('Y-m-d H:i:s'),
                ),
                'deleted' => array(
                    'type' => 'tinyint',
                    'default' => 0,
                ),
            );

            $forge->add_field($fields);
            $forge->add_key($this->field_id, TRUE);
            $attributes = array('ENGINE' => 'myisam');
            $forge->create_table($table, TRUE, $attributes);
        }

        //===========field exists
        $aSql = array();
        if (!$this->db_main->field_exists('code', $table)) {
            $aSql[] = "ALTER TABLE `{$table_name}` ADD `code` char(10) NULL 
			COMMENT 'kode random', ADD INDEX (`code`)";
        }
        if (!$this->db_main->field_exists('title', $table)) {
            $aSql[] = "ALTER TABLE `{$table_name}` ADD `title` varchar(255) NULL 
			COMMENT 'nama', ADD INDEX (`title`)";
        }
        if (!$this->db_main->field_exists('detail', $table)) {
            $aSql[] = "ALTER TABLE `{$table_name}` ADD `detail` Text NULL 
			COMMENT 'detail' ";
        }

        foreach ($aSql as $sql) {
            $this->db_main->query($sql);
        }
    }

}
