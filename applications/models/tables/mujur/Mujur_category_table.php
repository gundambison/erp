<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Settings_table
 *
 * @author R700
 */
class Mujur_category_table extends CI_Model {

    public $table;
    public $db_main;
    public $field_id;

    public function __construct() {
        parent::__construct();
        $this->table = 'category';
        $this->field_id = 'cat_id';
        $this->db_main = $this->load->database('main', TRUE);
        $this->created_table();
    }

    public function tablename() {
        return $this->db_main->dbprefix($this->table);
    }

    function created_table() {
        if (ENVIRONMENT != 'development' || !is_local()) {
            return TRUE;
        }
        if (is_local()) {
            log_add('Mujur_category_table  ', 'table');
        }
        $table = $this->table;
        $table_name = $this->db_main->dbprefix($table);
        $aSql = array();
        if (!$this->db_main->table_exists($table)) {
            $forge = $this->load->dbforge($this->db_main, TRUE);
            //------------
            $fields = array(
                $this->field_id => array(
                    'type' => 'BIGINT',
                    'auto_increment' => TRUE
                ),
                'updated_at' => array(
                    'type' => 'timestamp',
                ),
                'created' => array(
                    'type' => 'timestamp',
                    'default' => date('Y-m-d H:i:s'),
                ),
                'deleted_at' => array(
                    'type' => 'datetime',
                    'default' => NULL,
                ),
            );

            $forge->add_field($fields);
            $forge->add_key($this->field_id, TRUE);
            $attributes = array('ENGINE' => 'myisam');
            $forge->create_table($table, TRUE, $attributes);
            $aSql[] = "ALTER TABLE `{$table_name}` CHANGE "
                    . "`created` `created_at` TIMESTAMP NOT NULL "
                    . "DEFAULT CURRENT_TIMESTAMP COMMENT 'waktu dibuat';";
        }

        //===========field exists

        if (!$this->db_main->field_exists('cat_code', $table)) {
            $aSql[] = "ALTER TABLE `{$table_name}` ADD `cat_code` char(30) NULL 
			COMMENT 'kode random', ADD INDEX (`cat_code`)";
        }
        if (!$this->db_main->field_exists('cat_name', $table)) {
            $aSql[] = "ALTER TABLE `{$table_name}` ADD `cat_name` varchar(255) NULL 
			COMMENT 'nama', ADD INDEX (`cat_name`)";
        }
        if (!$this->db_main->field_exists('cat_detail', $table)) {
            $aSql[] = "ALTER TABLE `{$table_name}` ADD `cat_detail` Text NULL 
			COMMENT 'detail' ";
        }

        if (!$this->db_main->field_exists('cat_parent', $table)) {
            $aSql[] = "ALTER TABLE `{$table_name}` ADD `cat_parent` bigint  NULL 
			COMMENT 'parent', ADD INDEX (`cat_parent`)";
        }

        foreach ($aSql as $sql) {
            $this->db_main->query($sql);
            if (is_local()) {
                log_add('sql:' . $sql, 'table');
            }
        }
    }

}
