<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Settings_table
 *
 * @author R700
 */
class Mujur_users_dep_table extends CI_Model {

    public $table;
    public $db_main;
    public $field_id;

    public function __construct() {
        parent::__construct();
        $this->table = 'users_departement';
        $this->field_id = 'id'; //???
        $this->db_main = $this->load->database('main', TRUE);
        $this->created_table();
    }

    public function tablename() {
        return $this->db_main->dbprefix($this->table);
    }

    function created_table() {
        if (ENVIRONMENT != 'development' || !is_local()) {
            return TRUE;
        }
        if (is_local()) {
            log_add('Mujur_category_table  ', 'table');
        }
        $table = $this->table;
        $table_name = $this->db_main->dbprefix($table);
        $aSql = array();
        //============SUDAH TERSEDIA
        //===========field exists

        if (!$this->db_main->field_exists('ud_allow', $table)) {
            $aSql[] = "ALTER TABLE `{$table_name}` ADD `ud_allow` tinyint  default 0 
			COMMENT '1: allow', ADD INDEX (`ud_allow`)";
        }


        foreach ($aSql as $sql) {
            $this->db_main->query($sql);
            if (is_local()) {
                log_add('sql:' . $sql, 'table');
            }
        }
    }

}
