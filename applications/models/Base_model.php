<?php

class Base_model extends CI_Model {

    public $tables, $tables_id, $version;
    public $db_main, $tables_realname;

    public function __construct() {
        parent::__construct();
        $this->version = '0.1';
        $this->db_main = $this->load->database('main', TRUE);

    }

    public function drivers($driver_core, $driver_name, $func_name = 'executed', $params = array()) {
        $CI = & get_instance();
        $CI->load->driver($driver_core);
        $result = $CI->{$driver_core}->{$driver_name}->{$func_name}($params);
        return $result;
    }
    
    public function log_backup()
    {
        $dbs = $this->load->database('logs', TRUE);
        $table=$this->tables_realname['logs'];
        $sql="insert into `{$table}_back` select l1.* from `{$table}` l1 "
        . "left join `{$table}_back` l2 on l1.l_id=l2.l_id where l2.l_id is null";
        $dbs->query($sql);
        $now=date("Y-m-d H:i:s",strtotime("-12 hours"));
        $sql="delete from `{$table}` where modified <'$now'";
        $dbs->query($sql);
        //die($dbs->last_query());
    }

    public function log_add($text = '', $type = 'other', $cat = 0) {
        $log_allow = config_load('logs');
		if($log_allow!==TRUE) return FALSE;
		
        $header = date("Y-m-d H:i:s") . "\t" . strtoupper($type) . "\t";
        if($log_allow=='') return FALSE;
        
        if (is_array($text)) {
            $text = json_encode($text);
        }
/*
        $dbs = $this->load->database('logs', TRUE);
        $data['detail'] = $text;
        $data['type'] = $type;

        $dbs->insert($this->tables['logs'], $data);
*/
        if (is_local()) {
            $filename = date("Ymd") . '.txt';

            error_log($header . $text . "\n", 3, 'logs/' . $filename);
            return TRUE;
        }

        $folder = config_load('logs_folder');
        $folder_log = isset($folder[$cat]) ? $folder[$cat] . "/" : $folder[0] . "/";
        $folder_date = date("Ym") . "/";
        $filename = date("YmdH");
        $file = core_folder() . $folder_log . $folder_date . $filename . ".txt";
       
        
        if ($log_allow !== TRUE) {
            log_message('debug', $type . "\t" . $text);

            return FALSE;
        }

 $myfolder =  $folder_log . $folder_date;
        //die($myfolder);//core_folder() .
        create_folder_is_not_valid($myfolder); 
        file_put_contents($file, $header . $text . "\n", FILE_APPEND);

//    log_message('debug', $type . "\t" . $text);
    }

//======================DATABASE=====================
    private function db_insert($table_header, $data) {
        $this->db_main->insert($this->tables[$table_header], $data);
        log_add($this->db_main->last_query(), "insert", "db");
        return $this->db_main->insert_id();
    }

    private function db_fetch($one = FALSE) {
        $res = $this->db_main->get();
        log_add($this->db_main->last_query(), "fetch", "db");
        return $one ? $res->row_array() : $res->result_array();
    }

    private function db_query() {
        $res = $this->db_main->get();
        log_add($this->db_main->last_query(), "query", "db");
        return $res;
    }

    private function db_update($table_header, $input, $where) {
        $res = $this->db_main->update($this->tables[$table_header], $input, $where);
        log_add($this->db_main->last_query(), "fetch", "db");
        return $res;
    }

//--------------------MAIN---------------------------
    public function _new($data) {
        $data['deleted'] = 0;
        $data['created'] = date('Y-m-d H:i:s');
        $data[$this->tables_id['main']] = db_id('base', 'new');
        return $this->db_insert('main', $data);
    }

    function _get_data($filter = array(), $limit = 10, $start = 0, $count = FALSE, $debug = FALSE) {
        $head_table = 'main';
        $this->db_main->reset_query();
        $this->db_main->from($this->tables[$head_table]);

        if (isset($filter['get_all'])) {
            $this->db_main->where($filter['get_all'][0], $filter['get_all'][1]);
        }

        if (isset($filter['where_field'])) {
            $this->db_main->where($filter['where_field']);
        }
        if (isset($filter['where_like_field'])) {
            $this->db_main->like($filter['where_like_field']);
        }

        if (!isset($filter['show_all'])) {
            $this->db_main->where('deleted', 0);
        }

        //==============COUNT=============
        if ($count) {
            $this->db_main->select("count({$this->tables_id[$head_table]}) c");
            $data = $this->db_fetch(TRUE);
            return $data['c'];
        }

        //==================SHOW==========    
        if (isset($filter['show_fields'])) {
            $this->db_main->select($filter['show_fields']);
        }
        //================order by=== kondisikan
        if (!isset($filter['order_by'])) {
            $this->db_main->order_by('id', 'desc');
        } else {
            $this->db_main->order_by($filter['order_by'][0], $filter['order_by'][1]);
        }

        $this->db_main->limit($limit, $start);

        $data = $this->db_fetch();
        if ($debug) {
            $sql = $this->db_main->last_query();
            return array('data' => $data, 'sql' => $sql, 'filter' => $filter);
        } else {
            return $data;
        }
    }

    function _update($input, $where) {
        $this->db_update('main', $input, $where);
        return TRUE;
    }

    function _get_id($id = NULL, $field = 'id') { //rubah sesuai kebutuhan //
        if ($id == NULL)
            return FALSE;

        $filter['where_field'] = array($field => $id);
        $data = $this->_gets($filter, 1, 0, FALSE, FALSE);
        //    return $data;
        return $data[0] ? $data[0] : FALSE;
    }

}
