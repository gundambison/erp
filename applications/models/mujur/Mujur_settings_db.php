<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . '/core/Model_core.php';

class Mujur_settings_db extends Model_core {

    public $tables, $tables_id, $tables_realname;
    public $db_main;

    public function __CONSTRUCT() {
        $this->db_main = $this->base_model->db_main;
        parent::__construct();
        $this->tables = array(
            'access' => 'mujur/mujur_access', 
            'branch' => 'mujur/mujur_branch',
            'dep' => 'mujur/mujur_dep',
            'user_dep' => 'mujur/mujur_users_dep',
            'user' => 'mujur/mujur_users',
            'menu' => 'mujur/mujur_menu',
            'category' => 'mujur/mujur_category',
            'main' => 'settings'
        );

        $this->list_tables();
        if (is_local()) {
            log_add('mujur_settings_db list table: ' . json_encode($this->all_tables()), 'model');
        }
    }

    public function all_tables() {
        return array($this->tables, $this->tables_id, $this->tables_realname);
    }

//------------------MAIN------------------
    public function _gets($filter = array(), $limit = 10, $start = 0, $count = FALSE, $debug = FALSE) {
        $head_table = 'main';
        $this->db_main->reset_query();
        $this->db_main->from($this->tables[$head_table]);
        log_add("model filter:" . json_encode($filter), "_gets");
        if (isset($filter['get_all'])) {
            $this->db_main->where($filter['get_all'][0], $filter['get_all'][1]);
        }

        if (isset($filter['where_field'])) {
            $this->db_main->where($filter['where_field']);
        }
        if (isset($filter['where_like_field'])) {
            $this->db_main->like($filter['where_like_field']);
        }
//====DATATABLES=====================
        if (isset($filter['datatable_search'])) {
            if (strlen($filter['datatable_search']) > 3) {
                /*                 * *    
                  $this->db_main->group_start()
                  ->or_like("d1.dep_code" , strtoupper($filter['datatable_search']) )

                  ->group_end();
                 * * */
            } else {
                unset($filter['datatable_search']);
            }
        }

        if (!isset($filter['show_all'])) {
            $this->db_main->where('deleted', 0);
        }

        //==============COUNT=============
        if ($count) {
            $this->db_main->select("count({$this->tables_id[$head_table]}) c");
            $data = $this->db_fetch(TRUE);
            return $data['c'];
        }

        //==================SHOW==========    
        if (isset($filter['show_fields'])) {
            $this->db_main->select($filter['show_fields']);
        }
        //================order by=== kondisikan
        if (!isset($filter['order_by'])) {
            $this->db_main->order_by('id', 'desc');
        } else {
            $this->db_main->order_by($filter['order_by'][0], $filter['order_by'][1]);
        }

        $this->db_main->limit($limit, $start);
        $data = $this->db_fetch();
        log_add("model limit: {$start},{$limit} |total:" . count($data), "_gets");
        if ($debug) {
            $sql = $this->db_main->last_query();
            return array('data' => $data, 'sql' => $sql, 'filter' => $filter);
        } else {
            return $data;
        }
    }

    public function _counts($filter = array()) {
        $count = $this->_gets($filter, 1, 0, TRUE); //sesuaikan dgn tujuan gets
        log_add("model count total:{$count} |filter:" . json_encode($filter), "_count");
        return $count;
    }

    public function _gets_field($id = NULL, $fields = NULL, $max = 25) {
        if ($id == NULL) {
            return FALSE;
        }
        $head_table = 'main';
        if ($fields == NULL) {
            $fields = $this->tables_id[$head_table]; //rubah ke id
        }
        $filter = array('where_field' => array($fields, $id));
        log_add("model get_field filter:" . json_encode($filter), "_gets_field");
        $data = $this->_gets($filter, $max, 0, FALSE, FALSE); //sesuaikan dgn tujuan gets
        // return $data;
        return isset($data[0]) ? $data : FALSE; //multi array
    }

    public function _new($data = array()) {
        $head_table = 'main';
        $fields = $this->tables_id[$head_table];
        $input[$fields] = $id = db_id($head_table);
//==========Bila ada input yang ingin dimasukkan========


        $this->db_main->insert($this->tables[$head_table], $input);
        $id_new = $this->db_main->insert_id();
        log_add("model new id:{$id_new} |data:" . count($data), "_new");
        return $id_new;
    }

    public function _update($input = FALSE, $where = FALSE) {
        if (!$input || !$where) {
            return FALSE;
        }

        $head_table = 'main';
        log_add("model update where:" . json_encode($where) . " |data:" . array_keys($input), "_update");
        $this->db_update($head_table, $input, $where);
        return TRUE;
    }

//-----------------CABANG---------------
    function branch_gets($filter = array(), $limit = 10, $start = 0, $count = FALSE, $debug = FALSE) {
        $table = $this->tables['branch'];
        $this->db_main
                ->from("$table  b1");
        log_add("model |branch |filter:" . json_encode($filter), "_gets");
        //--------------SEARCH----------------------\
        if (isset($filter['id'])) {
            if (is_local())
                log_add('branch_gets filter id', 'db');
            $this->db_main->where("b1." . $this->tables_id['branch'], $filter['id']);
        }

        if (isset($filter['where_field'])) {
            if (is_local())
                log_add('branch_gets where ' . json_encode($filter['where_field']), 'db');
            $this->db_main->where("b1." . $filter['where_field'][0], $filter['where_field'][1]);
        }
        if (isset($filter['simple_where'])) {
            if (is_local())
                log_add('branch_gets simple_where ' . json_encode($filter['simple_where']), 'db');
            $this->db_main->where("b1." . $filter['simple_where']);
        }
        if (isset($filter['no_parent_id'])) {
            if (is_local())
                log_add('branch_gets no_parent_id ', 'db');
            $this->db_main->where("b1.branch_parent is NULL")
			->select("b1.branch_id id, b1.branch_code code, b1.branch_name name");
            $no_select = TRUE;
        }

        if (isset($filter['datatable_search'])) {
            if (is_local())
                log_add('branch_gets datatable ' . json_encode($filter['datatable_search']), 'db');
            if (strlen($filter['datatable_search']) > 3) {
                if (is_local())
                    log_add('branch_gets datatable start ' . json_encode($filter['datatable_search']), 'db');

                $this->db_main->group_start()
                        ->or_like("b1.branch_code", strtoupper($filter['datatable_search']))
                        ->or_like("b1.branch_detail", strtoupper($filter['datatable_search']))
                        ->or_like("b1.branch_name", strtoupper($filter['datatable_search']))
                        ->group_end();
            } else {
                if (is_local()){
                    log_add('branch_gets datatable not start', 'db');
				}
                unset($filter['datatable_search']);
            }
        }

        if (!isset($filter['show_all'])) {
            if (is_local())
                log_add('branch_gets show_all ', 'db');
            $this->db_main->where('b1.deleted_at', NULL);
            //$this->db_main->or_where('deleted', NULL);
        }

        //--------------COUNT==TRUE-----------------
        if ($count) {
            if (is_local())
                log_add('branch_gets count ' . json_encode($filter), 'db');
            $this->db_main->select("count(b1.{$this->tables_id['branch']}) c");
            $data = $this->db_fetch(TRUE);
            return $data['c'];
        }

        //=============ORDER==================
		if(isset($filter['no_order'])){
			$order_done=TRUE;
		}
        if (isset($filter['simple_order_by'])&& !isset($order_done)) {
            if (is_local()) {
                log_add('branch_gets order(1) ' . json_encode($filter['simple_order_by']), 'db');
            }
            $this->db_main->order_by($filter['simple_order_by']);
			$order_done=TRUE;
        }

        if (isset($filter['order_dir']) && isset($filter['order_by']) && !isset($order_done)) {
            if (is_local()) {
                log_add('branch_gets order_dir ' . json_encode($filter['order_dir']), 'db');
            }
            $this->db_main->order_by($filter['order_by'], $filter['order_dir']);
        }

        //--------------Limit-----------------
        $this->db_main->limit($limit, $start);
        //========join parent
        if (isset($filter['datatable_search'])) {
            if (is_local()) {
                log_add('branch_gets datatable_search (select)', 'db');
            }
            $this->db_main
                    ->select("b1.*, '-' as branch_nama_parent ");
        } elseif (!isset($no_select)) {
            if (is_local())
                log_add('branch_gets no_select(1) ', 'db');
            $this->db_main
                    ->select("b1.*, b2.branch_name branch_nama_parent")
                    ->join("$table b2", "b1.branch_parent=b2.branch_id", "left");
        }else {
            if (is_local())
                log_add('branch_gets no_select(2) ', 'db');
            $this->db_main->join("$table b2", "b1.branch_parent=b2.branch_id", "left");
        }

        $data = $this->db_fetch();
        log_add("model |branch |limit: {$start},{$limit} |total:" . count($data), "_gets");

//=============DEBUG=============
        if ($debug) {
            $sql = $this->db_main->last_query();
            return array('data' => $data, 'sql' => $sql, 'filter' => $filter);
        } else {
            return $data;
        }
    }

    function branch_counts($filter = array()) {
        $count = $this->branch_gets($filter, 1, 0, TRUE); //sesuaikan dgn tujuan gets
        log_add("model count total:{$count} |filter:" . json_encode($filter), "_count");
        return $count;
    }

    function branch_total() {
        return $this->branch_counts();
    }

    function branch_get_id($id = NUll) {
        if ($id == NULL) {
            return FALSE;
        }
        $filter = array('id' => $id,'no_order'=>TRUE);
        $data = $this->branch_gets($filter, 1, 0, FALSE, FALSE);
        //    return $data;
        return isset($data[0]) ? $data[0] : FALSE;
    }

    function branch_get_by_field($id = NULL, $fields = 'id') {
        $filter = array('where_field' => array($fields, $id), 'no_order'=>TRUE);
        log_add("model |branch |get_field filter:" . json_encode($filter), "_gets_field");
        $data = $this->branch_gets($filter, 1, 0, FALSE, FALSE);
        // return $data;
        return isset($data[0]) ? $data : FALSE;
    }

    function branch_new($input) {
        if (isset($input['branch_code'])) {
            $input['branch_code'] = strtoupper($input['branch_code']);
        }

        $input[$this->tables_id['branch']] = $id = db_id('branch');
        $this->db_main->insert($this->tables['branch'], $input);
        log_add("model new id:{$id } |data:" . count($input), "_new");
        return $this->db_main->insert_id();
		
    }

    function branch_update($input = NULL, $where = NULL) {
        //$sql = $this->db_main->update_string($this->tables['branch'], $input, $where);
        //log_add("branch_update sql:".json_encode($sql));
        //$this->db_main->query($sql);
        if (isset($input['branch_code']))
            $input['branch_code'] = strtoupper(trim($input['branch_code']));
        if (isset($input['branch_name']))
            $input['branch_name'] = strtoupper(trim($input['branch_name']));

        log_add("model |branch |update where:" . json_encode($where) . " |data:" . json_encode(($input)), "_update");
        $this->db_update('branch', $input, $where);
        return TRUE;
    }

    function branch_select() {
        $data = $this->branch_gets();
        //return $this->db_main->last_query();
        $select = array();
        foreach ($data as $row) {
            $select[$row['cat_code']] = $row['cat_title'];
        }

        return $select;
    }

    function branch_user_blank($id) {
		return TRUE;
        $head_table = 'user';
        $table1 = $this->tables_realname[$head_table];
        $table2 = $this->tables_realname['user_branch'];
        $sql = "insert into {$table2} (ud_user, ud_branchartement) 
		select u.id,'$id' from {$table1} u 
		left join 
			{$table2} ud on u.id=ud.ud_user and ud_branchartement=$id
			where ud.ud_user is NULL 
			limit 500";
        if (is_local()) {
            log_add("branch_user_blank\tsql:" . $sql, 'model');
        }
        $this->db_query($sql);
        return TRUE;
    }
//-----------------DEPARTEMENT---------------
    function dep_gets($filter = array(), $limit = 10, $start = 0, $count = FALSE, $debug = FALSE) {
        $table = $this->tables['dep'];
        $this->db_main
                ->from("$table d1");
        log_add("model |dep |filter:" . json_encode($filter), "_gets");
        //--------------SEARCH----------------------\
        if (isset($filter['id'])) {
            if (is_local())
                log_add('dep_gets filter id', 'db');
            $this->db_main->where("d1." . $this->tables_id['dep'], $filter['id']);
        }

        if (isset($filter['where_field'])) {
            if (is_local())
                log_add('dep_gets where ' . json_encode($filter['where_field']), 'db');
            $this->db_main->where("d1." . $filter['where_field'][0], $filter['where_field'][1]);
        }
        if (isset($filter['simple_where'])) {
            if (is_local())
                log_add('dep_gets simple_where ' . json_encode($filter['simple_where']), 'db');
            $this->db_main->where("d1." . $filter['simple_where']);
        }
        if (isset($filter['no_parent_id'])) {
            if (is_local())
                log_add('dep_gets no_parent_id ', 'db');
            $this->db_main->where("d1.dep_parent is NULL")->select("d1.dep_id id, d1.dep_code code, d1.dep_name name");
            $no_select = TRUE;
        }

        if (isset($filter['datatable_search'])) {
            if (is_local())
                log_add('dep_gets datatable ' . json_encode($filter['datatable_search']), 'db');
            if (strlen($filter['datatable_search']) > 3) {
                if (is_local())
                    log_add('dep_gets datatable start ' . json_encode($filter['datatable_search']), 'db');

                $this->db_main->group_start()
                        ->or_like("d1.dep_code", strtoupper($filter['datatable_search']))
                        ->or_like("d1.dep_detail", strtoupper($filter['datatable_search']))
                        ->or_like("d1.dep_name", strtoupper($filter['datatable_search']))
                        ->group_end();
            } else {
                if (is_local())
                    log_add('dep_gets datatable not start', 'db');
                unset($filter['datatable_search']);
            }
        }

        if (!isset($filter['show_all'])) {
            if (is_local())
                log_add('dep_gets show_all ', 'db');
            $this->db_main->where('d1.deleted_at', NULL);
            //$this->db_main->or_where('deleted', NULL);
        }

        //--------------COUNT==TRUE-----------------
        if ($count) {
            if (is_local())
                log_add('dep_gets count ' . json_encode($filter), 'db');
            $this->db_main->select("count(d1.{$this->tables_id['dep']}) c");
            $data = $this->db_fetch(TRUE);
            return $data['c'];
        }

        //=============ORDER==================
        if (isset($filter['simple_order_by'])) {
            if (is_local()) {
                log_add('dep_gets order(1) ' . json_encode($filter['simple_order_by']), 'db');
            }
            $this->db_main->order_by($filter['simple_order_by']);
        }

        if (isset($filter['order_dir']) && isset($filter['order_by']) && !isset($order_done)) {
            if (is_local()) {
                log_add('dep_gets order_dir ' . json_encode($filter['order_dir']), 'db');
            }
            $this->db_main->order_by($filter['order_by'], $filter['order_dir']);
        }

        //--------------Limit-----------------
        $this->db_main->limit($limit, $start);
        //========join parent
        if (isset($filter['datatable_search'])) {
            if (is_local()) {
                log_add('dep_gets datatable_search (select)', 'db');
            }
            $this->db_main
                    ->select("d1.*, '-' as dep_nama_parent ");
        } elseif (!isset($no_select)) {
            if (is_local())
                log_add('dep_gets no_select(1) ', 'db');
            $this->db_main
                    ->select("d1.*, d2.dep_name dep_nama_parent")
                    ->join("$table d2", "d1.dep_parent=d2.dep_id", "left");
        }else {
            if (is_local())
                log_add('dep_gets no_select(2) ', 'db');
            $this->db_main->join("$table d2", "d1.dep_parent=d2.dep_id", "left");
        }

        $data = $this->db_fetch();
        log_add("model |dep |limit: {$start},{$limit} |total:" . count($data), "_gets");

//=============DEBUG=============
        if ($debug) {
            $sql = $this->db_main->last_query();
            return array('data' => $data, 'sql' => $sql, 'filter' => $filter);
        } else {
            return $data;
        }
    }

    function dep_counts($filter = array()) {
        $count = $this->dep_gets($filter, 1, 0, TRUE); //sesuaikan dgn tujuan gets
        log_add("model count total:{$count} |filter:" . json_encode($filter), "_count");
        return $count;
    }

    function dep_total() {
        return $this->dep_counts();
    }

    function dep_get_id($id = NUll) {
        if ($id == NULL) {
            return FALSE;
        }
        $filter = array('id' => $id);
        $data = $this->dep_gets($filter, 1, 0, FALSE, FALSE);
        //    return $data;
        return isset($data[0]) ? $data[0] : FALSE;
    }

    function dep_get_by_field($id = NULL, $fields = 'id') {
        $filter = array('where_field' => array($fields, $id));
        log_add("model |dep |get_field filter:" . json_encode($filter), "_gets_field");
        $data = $this->dep_gets($filter, 1, 0, FALSE, FALSE);
        // return $data;
        return isset($data[0]) ? $data : FALSE;
    }

    function dep_new($input) {
        if (isset($input['dep_code'])) {
            $input['dep_code'] = strtoupper($input['dep_code']);
        }

        $input[$this->tables_id['dep']] = $id = db_id('dep');
        $this->db_main->insert($this->tables['dep'], $input);
        log_add("model new id:{$id } |data:" . count($input), "_new");
        return $this->db_main->insert_id();
    }

    function dep_update($input = NULL, $where = NULL) {
        //$sql = $this->db_main->update_string($this->tables['dep'], $input, $where);
        //log_add("dep_update sql:".json_encode($sql));
        //$this->db_main->query($sql);
        if (isset($input['dep_code']))
            $input['dep_code'] = strtoupper(trim($input['dep_code']));
        if (isset($input['dep_name']))
            $input['dep_name'] = strtoupper(trim($input['dep_name']));

        log_add("model |dep |update where:" . json_encode($where) . " |data:" . json_encode(($input)), "_update");
        $this->db_update('dep', $input, $where);
        return TRUE;
    }

    function dep_select() {
        $data = $this->dep_gets();
        //return $this->db_main->last_query();
        $select = array();
        foreach ($data as $row) {
            $select[$row['cat_code']] = $row['cat_title'];
        }

        return $select;
    }

    function dep_user_blank($id) {
        $head_table = 'user';
        $table1 = $this->tables_realname[$head_table];
        $table2 = $this->tables_realname['user_dep'];
        $sql = "insert into {$table2} (ud_user, ud_departement) 
		select u.id,'$id' from {$table1} u 
		left join 
			{$table2} ud on u.id=ud.ud_user and ud_departement=$id
			where ud.ud_user is NULL 
			limit 500";
        if (is_local()) {
            log_add("dep_user_blank\tsql:" . $sql, 'model');
        }
        $this->db_query($sql);
        return TRUE;
    }

//==================OTHER==============
    function all_tables0() {
        $util = $this->load->dbutil($this->db_main, TRUE);
        return $this->db_main->list_tables();
    }

    function example_file($file = 'model') {

        $ar = explode("/", $_SERVER['SCRIPT_FILENAME']);
        unset($ar[count($ar) - 1]);
        $doc_root = implode("/", $ar) . "/";

        //die(print_r($_SERVER));
        if ($file == 'model') {
            $file_read = 'assets/support/example_db.php';
        }
        if ($file == 'table') {
            $file_read = 'assets/support/example_table.php';
        }

        $str = file_get_contents($doc_root . $file_read);
        return $str;
    }

//------------------Category------------------
    public function cat_gets($filter = array(), $limit = 10, $start = 0, $count = FALSE, $debug = FALSE) {
        //=========CLEAN parent to 0
        $input = array('cat_parent' => 0);
        $where = array("cat_parent" => NULL);
        $this->cat_update($input, $where);
        //==============START============
        $head_table = 'category';
        $table = $this->tables[$head_table];
        $this->db_main->reset_query();
        $this->db_main->from($this->tables[$head_table] . " as c1");
        log_add("model filter:" . json_encode($filter), "_gets");
        $fields = $this->tables_id[$head_table];
        if (isset($filter[$fields])) {
            $this->db_main->where("c1." . $fields, $filter[$fields]);
            unset($fields);
        }

        if (isset($filter['get_all'])) {
            $this->db_main->where($filter['get_all'][0], $filter['get_all'][1]);
        }
        if (isset($filter['cat_code'])) {
            $this->db_main->where('cat_code', $filter['cat_code']);
        }
        if (isset($filter['cat_parent'])) {
            $this->db_main->where('c1.cat_parent', $filter['cat_parent']);
        }
        if (isset($filter['cat_parent_code'])) {
            $this->db_main->where('c2.cat_code', $filter['cat_parent_code']);
        }
        if (isset($filter['no_parent_id'])) {
            $this->db_main->where('c1.cat_parent', 0);
        }
        if (isset($filter['where_field'])) {
            $this->db_main->where($filter['where_field']);
        }
        if (isset($filter['where_like_field'])) {
            $this->db_main->like($filter['where_like_field']);
        }
//====DATATABLES=====================
        if (isset($filter['datatable_search'])) {
            if (strlen($filter['datatable_search']) > 3) {
                /*                 * *    
                  $this->db_main->group_start()
                  ->or_like("d1.dep_code" , strtoupper($filter['datatable_search']) )

                  ->group_end();
                 * * */
            } else {
                unset($filter['datatable_search']);
            }
        }

        if (!isset($filter['show_all'])) {
            $this->db_main->where('c1.deleted_at', NULL);
        }

        //==============COUNT=============
        if ($count) {
            //c1.{$this->tables_id[$head_table]}
            $this->db_main->select("count(*) c");
            if (isset($filter['cat_parent_code'])) {
                $this->db_main->join("$table c2", "c1.cat_parent=c2.cat_id", "left");
            }
            $data = $this->db_fetch(TRUE);
            return $data['c'];
        }

        //==================SHOW==========    
        if (isset($filter['show_fields'])) {
            $this->db_main->select($filter['show_fields']);
        }
        //================order by=== kondisikan
        if (isset($filter['order_dir']) && isset($filter['order_by']) && !isset($order_done)) {
            if (is_local()){
                log_add('cat_gets order_dir ' . json_encode($filter['order_dir']), 'db');
            }
            $this->db_main->order_by($filter['order_by'], $filter['order_dir']);
            
        } elseif (isset($filter['order_by']) && is_array($filter['order_by'])) {
            $this->db_main->order_by($filter['order_by'][0], $filter['order_by'][1]);
            if (is_local()) {
                log_add('order by (2):' . json_encode($filter['order_by']));
            }
            
        } elseif (isset($filter['order_by'])) {
            $this->db_main->order_by($filter['order_by']);
            if (is_local()) {
                log_add('order by (3):' . json_encode($filter['order_by']));
            }
        } else {
            $fields = $this->tables_id[$head_table];
            $this->db_main->order_by($fields, 'desc');
            if (is_local()) {
                log_add('order by ' . $fields);
            }
        }

        $this->db_main->limit($limit, $start);
        if (isset($filter['datatable_search'])) {
            if (is_local()) {
                log_add('cat_gets datatable_search (select)', 'db');
            }
            $this->db_main
                    ->select("c1.*, '-' as cat_nama_parent ");
        } elseif (!isset($no_select)) {
            if (is_local()) {
                log_add('cat_gets no_select(1) ', 'db');
            }
            $this->db_main
                    ->select("c1.*, c2.cat_name cat_nama_parent")
                    ->join("$table c2", "c1.cat_parent=c2.cat_id", "left");
        } else {
            if (is_local())
                log_add('dep_gets no_select(2) ', 'db');
            $this->db_main->join("$table c2", "c1.cat_parent=c2.cat_id", "left");
        }

        $data = $this->db_fetch();
        log_add("model limit: {$start},{$limit} |total:" . count($data), "_gets");
        if ($debug) {
            $sql = $this->db_main->last_query();
            return array('data' => $data, 'sql' => $sql, 'filter' => $filter);
        } else {
            return $data;
        }
    }

    public function cat_counts($filter = array()) {
        $count = $this->cat_gets($filter, 1, 0, TRUE); //sesuaikan dgn tujuan gets
        log_add("model count total:{$count} |filter:" . json_encode($filter), "_count");
        return $count;
    }

    public function cat_gets_field($id = NULL, $fields = NULL, $max = 25) {
        if ($id == NULL) {
            return FALSE;
        }
        $head_table = 'category';

        if ($fields == NULL) {
            $fields = $this->tables_id[$head_table]; //rubah ke id
        }
        $filter = array('where_field' => array($fields, $id));
        log_add("model get_field filter:" . json_encode($filter), "_gets_field");
        $data = $this->_gets($filter, $max, 0, FALSE, FALSE); //sesuaikan dgn tujuan gets
        // return $data;
        return isset($data[0]) ? $data : FALSE; //multi array
    }

    public function cat_new($data = array()) {
        $head_table = 'category';
        $fields = $this->tables_id[$head_table];
        $input[$fields] = $id = db_id($head_table);
//==========Bila ada input yang ingin dimasukkan========
        $total = $this->cat_total();
        $input['cat_code'] = rand_word(5) . dechex($total);

        $this->db_main->insert($this->tables[$head_table], $input);
        $id_new = $this->db_main->insert_id();
        log_add("model new id:{$id_new} |data:" . count($data), "_new");
        return $id_new;
    }

    public function cat_update($input = FALSE, $where = FALSE) {
        if (!$input || !$where) {
            return FALSE;
        }

        $head_table = 'category';
        log_add("model update where:" . json_encode($where) . " |data:" .
                json_encode(array_keys($input)), "_update");
        $this->db_update($head_table, $input, $where);
        return TRUE;
    }

    function cat_get_id($id = NUll) {
        if ($id == NULL) {
            return FALSE;
        }
        $filter = array('cat_id' => $id);
        $data = $this->cat_gets($filter, 1, 0, FALSE, FALSE);
        //    return $data;
        return isset($data[0]) ? $data[0] : FALSE;
    }

    function cat_total() {
        return $this->cat_counts();
    }

//----------------
}
