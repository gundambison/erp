<?php

class Settings_m extends CI_Model {

   function list_function() {
        $info = array(
            'detail' => array(
                'param' => 'id', 'return' => 'array'
            ),
            'new' => array(
                'param' => '', 'return' => "id\ndetail=>array"
            ),
        );
        $info['select'] = array(
            'param' => 'limit,total => gunakan kedua nya',
            'return' => "array"
        );
        $info['update'] = array(
            'param' => "id\ndata=>array",
            'return' => "array"
        );
        $info['total'] = array(
            'param' => 'filter=>array',
            'return' => "int"
        );
        $info['tables'] = array(
            'param' => "filters = array()\nlimit = 30\nstart = 0\ndebug = FALSE",
            'return' => "array"
        );
        $info['gets'] = array(
            'param' => "filters = array()\nlimit = 30\nstart = 0\ndebug = FALSE",
            'return' => "array"
        );
        $target = array(
            'category' 
        );
        ksort($info);
        return array('func' => $info, 'target' => $target);
    }

    function _run($target, $action, $params = array()) {
        $params['action'] = $action;
        $info = $this->list_function();
        log_local('target:' . $target . ' |service:' . print_r($params, 1));
        if (in_array($target, $info['target'])) {
            $return = $this->_local_api($target, $params);

            return params_exist($return, 'data', $return);
        } else {
            log_add('unknown target');
            return FALSE;
        }
    }

    private function _local_api($target, $params) {
		 //$params['token'] = my_token();

          
        /*  sebelum
$url = config_load('erp_api_url') . "/demo/setting/".$target;
          $result = _runApi($url, $params);
          return $result;
         */
		 
        $this->load->model('mujur/mujur_settings_db');
		$allow_type=array(
			'category'=>'cat'
		
		);
		if (!isset( $allow_type[$target])) {
            log_add('no action action:' . $type0);
			$respon['error'] = 'Unknown or Not Allowed';
			return $respon;

        }else{
			$type=$allow_type[$target];
		}
		
        $allow_action = array(
            'new', 'update', 'detail', 'gets', 'select', 'total', 'table'
        );
        $action = $params['action'];
        if (!in_array($action, $allow_action)) {
            log_add('no action action:' . $action);
            $respon['error'] = 'action unknown';
            //$this->result(FALSE, 'NOACT', "Action Unknown");
        } else {
            log_add('start action:' . $action, 'mod_loc_api');
            $respon = driver_run('erp', 'setting_' . $type, $type.'_' . $action, $params);
 
            log_local('setting_' . $type . " |" . $type.'_' . $action . ' res:' . print_r($respon, 1), 'mod_loc_api');
            log_add('end action:' . 'serv_' . $target . " |s" . '_' . $action, 'mod_loc_api');
        }

        $result['data'] = $respon;
        return $result;
    }

}