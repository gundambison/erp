<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function logConfig($text) {
    log_add($text, 'config');
}

function logCreate($text = '') {
    log_add($text);
    return TRUE;
}

function log_add($text = '', $type = 'debug', $cat = '') {
    $CI = & get_instance();
    $CI->load->model('base_model');
    $CI->base_model->log_add($text, $type, $cat);
}

function log_local($text = '', $type = 'local', $cat = '') {
    return is_local() ? log_add($text, $type . "_local", $cat) : FALSE;
}

function create_folder_is_not_valid($folders) {
    $log_win = TRUE;
    $ar = explode("/", $folders);
    $folder = '';
//	echo_r($ar);
    foreach ($ar as $v) {
        if ($v == '') {
            continue;
        }

        $folder .= $log_win ? "$v/" : "/$v";
        //echo "<br>$folder";
        if (!is_dir($folder)) {

            @mkdir($folder);
        }
    }
    return TRUE;
}
