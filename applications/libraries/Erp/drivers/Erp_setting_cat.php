<?php
//========= D:\php\erp\application\libraries/Erp/drivers/Erp_setting_cat.php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Erp_setting_cat
 * erp setting_cat
 * @author Auto Generate
 */
class Erp_setting_cat extends CI_Driver{

    private $urls, $privatekey, $db_main;
    public $CI;
    function __construct() {
        $CI = & get_instance();
        //$this->db_main = $CI->base_model->db_main;
        $CI->load->model('mujur/mujur_settings_db');
         if (is_local()) {
            log_add('erp| setting_cat', 'library');
        }
    }

    /**
     * Fungsi : cat_gets
     *
     * auto generate on 2018-07-01 00:22:33
     *
     * @param    type    $params     array
     *
     * @return    void
     */
    public function cat_gets($params=FALSE) {
		$CI = & get_instance();
        if (is_local()) {
            log_add('cat_get params ' . json_encode($params), 'db');
        }

        $limit = isset($params['limit']) ? $params['limit'] : 2;
        $start = isset($params['start']) ? $params['start'] : 0;
		$filter = isset($params['filters']) ? $params['filters']:array();
        $detail = $CI->mujur_settings_db->cat_gets($filter , $limit, $start)  ;
        $total =  $CI->mujur_settings_db->cat_gets($filter, 1, 0, TRUE)  ;
        $result = array(
            'detail' => $detail,
            'total' => $total,
            'params' => $params
        );
        $result['raw'] = isset($params['filters']) ? $CI->mujur_settings_db
                ->cat_gets($params['filters'], $limit, $start,FALSE,TRUE) : FALSE;

        return $result;
    }


    /**
     * Fungsi : se_new
     *
     * Kegunaan ketik disini
     *
     * @param    type    $params     array
     *
     * @return    void
     */
    public function se_new($params=FALSE) {

        $result    =    array();


        return $result;
    }

    /**
     * Fungsi : se_update
     *
     * Kegunaan ketik disini
     *
     * @param    type    $params     array
     *
     * @return    void
     */
    public function se_update($params=FALSE) {

        $result    =    array();


        return $result;
    }

    /**
     * Fungsi : se_detail
     *
     * Kegunaan ketik disini
     *
     * @param    type    $params     array
     *
     * @return    void
     */
    public function se_detail($params=FALSE) {

        $result    =    array();


        return $result;
    }

    /**
     * Fungsi : se_gets
     *
     * Kegunaan ketik disini
     *
     * @param    type    $params     array
     *
     * @return    void
     */
    public function se_gets($params=FALSE) {

        $result    =    array();


        return $result;
    }

    /**
     * Fungsi : se_select
     *
     * Kegunaan ketik disini
     *
     * @param    type    $params     array
     *
     * @return    void
     */
    public function se_select($params=FALSE) {

        $result    =    array();


        return $result;
    }

    /**
     * Fungsi : se_total
     *
     * Kegunaan ketik disini
     *
     * @param    type    $params     array
     *
     * @return    void
     */
    public function se_total($params=FALSE) {

        $result    =    array();


        return $result;
    }

/*     * MORE* */

}
