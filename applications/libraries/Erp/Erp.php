<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ERP extends CI_Driver_Library {

    public $driver_name;
    public $valid_drivers;
    public $CI;

    /*     * *
      Daftar Fungsi Yang Tersedia :
     * 	__construct()
     * * */

    function __construct() {
        $this->CI = $CI = & get_instance();
        $config_file = 'driver_erp';
        $driver_core = 'erp';

        $valid_drivers = config_load('drivers_' . $driver_core, $config_file);
		//echo_r($valid_drivers);
        $this->valid_drivers = $valid_drivers;
    }

}
