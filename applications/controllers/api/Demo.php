<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . '/core/API_core.php';

class Demo extends API_core {

    private $parent = 'erp';

    function __construct() {
        parent::__construct();
        header('Access-Control-Allow-Origin: *');
    }

 
	function setting_post($type0=NULL){
		if($type0==NULL){
			$this->result(FALSE, 'UTR', "Unknown Type Request");
		}
		$respon=array('params'=>$this->post());
		$allow_type=array(
			'category'=>'cat'
		
		);
		if (!isset( $allow_type[$type0])) {
			
            log_add('no action action:' . $type0);
            $this->result(FALSE, 'UONA', "Unknown or Not Allowed");
        } else {//
            $type = $allow_type[$type0];
        }

        $allow_action = array(
            'new', 'update', 'detail', 'gets'
        );
        $params = $this->post();
        $action = $this->post('action');


        if (!in_array($action, $allow_action)) {
            log_add('no action action:' . $action);
            $this->result(FALSE, 'NOACT', "Action Unknown");
        } else {
 
			$respon  = driver_run($this->parent, 'setting_'.$type, $type.'_' . $action, $params);
		}
		$this->result($respon);
	}
	
	function login_post($type0='user'){//user only
		$respon=array('params'=>$this->post());
		$type='user';
		$respon=array('params'=>$this->post());
		$allow_action = array(
            'gets'
        );
		$params = $this->post();
		
		//$this->allowed_client();
		$this->app_login();
        $action = $this->post('action');
		
		log_local(print_r($respon,1),'api_controllers');
        if (!in_array($action, $allow_action)) {
            log_add('no action action:' . $action);
            $this->result(FALSE, 'NOACT', "Action Unknown");
        } else {
			$respon  = driver_run($this->parent, 'login_'.$type, $type.'_' . $action, $params);
		}
		
		$this->result($respon);
	}
}
 
