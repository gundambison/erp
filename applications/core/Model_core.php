<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Model_core extends CI_Model {

    public function __CONSTRUCT() {
        parent::__construct();
        if (is_local()) {
            log_add('model core start', 'core');
        }
    }

    protected function list_tables() {
        $tables = $this->tables;
        if (is_local()) {
            log_add('model core load :' . json_encode($tables), 'core');
        }
        foreach ($tables as $key => $str) {
            $ar = explode("/", $str);
            $table = $ar[count($ar) - 1]; //nama terakhir
            $table_file = $str . "_table";
            $table_model_name = $table . "_table";
            if (is_local()) {
                log_add('model core load :' . $table_file, 'core');
            }
            $this->load->model('tables/' . $table_file);
            $this->tables[$key] = isset($this->$table_model_name->table) ? $this->$table_model_name->table : NULL;
            $this->tables_realname[$key] = isset($this->$table_model_name->table) ? $this->$table_model_name->tablename() : NULL;

            $this->tables_id[$key] = isset($this->$table_model_name->field_id) ? $this->$table_model_name->field_id : NULL;
        }
        if (is_local()) {
            log_add('table ' . print_r($this->tables, 1));
            log_add('table realname ' . print_r($this->tables_realname, 1));
            log_add('table id' . print_r($this->tables_id, 1));
        }
        return array($this->tables, $this->tables_realname, $this->tables_id);
    }

//======================DATABASE=====================
    protected function db_insert($table_header, $data) {
        if (is_local()) {
            log_add("insert to:{$table_header} |params:" . print_r($data, 1), "db");
        }
        $this->db_main->insert($this->tables[$table_header], $data);
        
        $id = $this->db_main->insert_id();
        if (is_local()) {
            log_add("insert to:{$table_header} |id:{$id} |sql:" . $this->db_main->last_query(), "db");
        }else{
            log_add($this->db_main->last_query(), "insert", "db");
        }
        return $id;
    }

    protected function db_fetch($one = FALSE) {
        if (is_local()) {
            log_add("db_fetch start ", "db");
        }

        $res = $this->db_main->get();
        
        if (is_local()) {
            log_add("fetch one:" . $one . " |sql:" . $this->db_main->last_query(), "db");
        }else{
            log_add($this->db_main->last_query(), "query", "db");
        }
        return $one ? $res->row_array() : $res->result_array();
    }

    protected function db_get() {
        if (is_local()) {
            log_add("db_get start ", "db");
        }
        $res = $this->db_main->get();
        
        if (is_local()) {
            log_add("query |sql:" . $this->db_main->last_query(), "db");
        }else{
            log_add($this->db_main->last_query(), "query", "db");
        }
        return $res;
    }

    protected function db_query($sql = FALSE) {
        $res = $this->db_main->query($sql);
        log_add($this->db_main->last_query(), "query", "db");
        if (is_local()) {
            log_add("query |sql:" . $this->db_main->last_query(), "db");
        }
        return $res;
    }

    protected function db_update($table_header, $input, $where) {
        $res = $this->db_main->update($this->tables[$table_header], $input, $where);
        log_add($this->db_main->last_query(), "query", "db");
        if (is_local()) {
            log_add("update |table:{$table_header} |input total:" . count($input) . " |where:" . json_encode($where) .
                    " |sql:" . $this->db_main->last_query(), "db");
        }
        return $res;
    }

}
